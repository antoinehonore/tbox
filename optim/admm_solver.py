from tbox.ml.utils import compute_nme
import numpy as np


def admm_solver(x, y, eps_o, mu, kmax):
    p, N = x.shape
    Q, N = y.shape
    Lam = np.zeros((Q, p))
    xxt = np.matmul(x, x.transpose())
    yxt = np.matmul(y, x.transpose())
    tmp = np.linalg.inv(xxt + 1 / mu * np.identity(p))
    Z = Lam
    cost = []

    for _ in range(kmax):
        O = np.matmul(yxt + (1 / mu) * (Z + Lam), tmp)
        Z = O - Lam
        nZ = np.linalg.norm(Z, 'fro')
        if nZ > eps_o:
            Z = Z * (eps_o / nZ)
        Lam = Lam + Z - O
        cost.append(compute_nme(y, np.matmul(O, x)))
        if len(cost) > 2 and cost[-2] <= cost[-1]:
            break

    if len(cost) == kmax:
        print("\n\n:::::WARNING:::: admm_solver Did not converged.\n\n")

    return O