"""
Created on 20 Mar 2018

@author: anthon
"""

import re, os, sys, pickle
from datetime import datetime, timedelta
import numpy as np
import zlib
from filelock import FileLock
import subprocess

def githash():
    """Get the current git commit short hash."""
    return subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).strip().decode("utf-8")

def unzip(T):
    """
    Turn list of constant sized tuples into list of list.

    Parameters
    ----------

    T : list of tuples. length n : [(e_11,...,e_1p), ..., (e_n1,...,e_np)]
    Tuples must be of constant size p.

    Returns
    -------
    List of p lists of length n : [[e_11, ... e_n1], ... [e_1p,...,e_np]].

    """
    return [list(x) for x in zip(*T)]


def makedirs(*args):
    for dirpath in args:
        if not os.path.isdir(dirpath):
            os.makedirs(dirpath)
    return 0


def abspath(*path):
    return os.path.join(*path)


def pkload(fname="a.out"):
    out = pickle.load(open(fname, "rb"))
    return out


def zpkload(fname="a.out"):
    return pickle.loads(zlib.decompress(pkload(fname=fname)[0]))


def pkdump(*args, fname="a.out"):
    pickle.dump(args, open(fname, "wb"))
    return 0


def zpkdump(*args, fname="a.out"):
    compressed = zlib.compress(pickle.dumps(args))
    pkdump(compressed, fname=fname)
    return 0


class Timer(object):
    """Timing several functions.

     Passed as arguments to the constructor.
     Functions must be wrapped into multiprocessing.partial objects

    Example:
        >>> fun1 = partial(fp1,arg11)
        >>> fun2 = partial(fp2,arg12)

        >>> timer=Timer(fun1,fun2)
        >>> timer()
        [ <function fun1 at 0x11bae7840> ] 1.825401 s
        [ <function fun2 at 0x11bae7840> ] 0.257025 s
        >>> timer.runtime
        [1.825401, 0.257025]

    """
    def __init__(self, *args):
        self.fun = [fun for fun in args]
        self.runtime = [0 for _ in range(len(self.fun))]
        #Will be replaced in the call
        self.start = datetime.now()
        self.finish = datetime.now()
        return


    def run(self, fun):
        self.start = datetime.now()
        out = fun()
        self.finish = datetime.now()
        return (self.finish - self.start).total_seconds()


    def __call__(self):
        for i, funcp in enumerate(self.fun):
            self.runtime[i] = self.run(funcp)
            print("[", str(funcp.func), "]", self.runtime[i], "s")
        return


def stringtoitem(item_str):
    """
    Tries to convert a string into the type of data it contains.
    Admissible types: None, int, float, datetime.datetime, datetime.timedelta
    :param item_str: String representing an admissible type
    :type item_str: String
    :return:  String cast into one of the admissible types
    :rtype: int, datetime.datetime, datetime.timedelta
    """

    date_fmt = "%Y-%m-%d %H:%M:%S"

    if isnumber(item_str):  # Find out if int or float
        if item_str.isnumeric():
            return int(item_str)
        else:
            return float(item_str)
    elif item_str == 'None':
        return None

    else:  # Strings
        try:
            return datetime.strptime(item_str, date_fmt)  # standard datetime
        except:
            try:  # Timedelta
                tmp = datetime.strptime(item_str, "%H:%M:%S")
                return timedelta(hours=tmp.hour, minutes=tmp.minute, seconds=tmp.second)
            except:  # Strings
                return item_str


def myrd(x, signi=3):
    """
    Element-wise rounding of elements in x.
    Call numpy.round() with default significative number of 3
    :param x: Array or list to round
    :type x: numpy.ndarray or list
    :param signi: Number of significative digits
    :type signi: int
    :return: Rounded array or List
    :rtype: numpy.ndarray or List
    """
    return np.round(x, signi)


def sort_wrt1(*args, reverse=False):
    assert(all([len(args[0]) == len(l) for l in args]))

    yx = list(zip(*args))
    yx.sort(reverse=reverse)

    argsout = [[0 for _ in range(len(args[0]))] for _ in range(len(args))]
    for ilist in range(len(argsout)):
        for ival in range(len(args[0])):
            argsout[ilist][ival]=yx[ival][ilist]
    return argsout


def percprint(i, n, every=5):
    """
    Prints progression percentage to stdout.
    :param i: iteration number
    :type i: int
    :param n: Total number of iteration
    :type n: int
    :param every: Print every `every`%
    :type every: int
    :return: nothing
    """
    perc_val = round(100 * (i+1) / n)
    if perc_val > 0 and perc_val % every == 0:
        pidprint("{} %".format(perc_val))
    return 0





def pidstr():
    """
    Writes the current process number to string
    :return: "pid: 'current pid' > "
    :rtype: String
    """
    return "pid: " + str(os.getpid()) + " > "


def pidprint(*arg):
    """
    Behaves like builtin `print` function, but append the output of pidstr() before writing to stdout.
    :return: Write to stdout
    """
    print(pidstr() + " ".join(map(str, arg)))
    return


def isnumber(s):
    """
    Tells if object in argument is a number or not. A nu
    :param s: object
    :return: True iif s is castable into a complex number
    :rtype: bool
    """
    try:
        complex(s)
    except ValueError:
        return False
    return True


def atoi(text):
    """
    Tries to convert string to int
    :param text: Input string
    :return: int contained in the input string is possible
    :rtype: int or String
    """
    return int(text) if text.isdigit() else text


def natural_keys(text):
    """
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    """
    return [atoi(c) for c in re.split('(\d+)', text)]


def ismember(a, b):
    """
    returns a list l such that a(l) = b
    """
    bind = {}
    for i, elt in enumerate(b):
        if elt not in bind:
            bind[elt] = i
    return [bind.get(itm, None) for itm in a]


def stat_pm_std(x, ndigits=3, latex=False, stat_fun=np.mean):
    """
    x in a list of float and the function returns a string like: mu +- std, all values rounded with 'signi' significant numbers
    """
    str_out =  str(np.round(stat_fun(x), ndigits)) + " +- " + str(np.round(np.std(x), ndigits))
    if latex:
        str_out = "$" + str_out.replace("+-", "\pm") + "$"

    return str_out


def dict2str(d):
    out_str = ''
    for k, v in d.items():
        out_str = out_str + "{}-{}__".format(k, v)
    # Remove last __
    out_str =   out_str[:-2]
    return out_str


def str2dict(str_in):
    kv = str_in.split("__")
    d = {}
    for x in kv:
        k, v = x.split("-")
        d[k] = atoi(v)
    return d


def dict2file(mydictionary, filename, verbose=False):
    """
    Writes dictionary to a specified file, "key1 : value1\nkey2 : value2\n..."
    :param mydictionary: Dictionary to write in file
    :type mydictionary: Dictionary
    :param filename: filename to write dictionary in
    :type filename: String
    :return: nothing
    """


    fout = open(filename, 'w', encoding='utf-8')
    for k, v in mydictionary.items():
        fout.write(str(k) + ' : ' + str(v) + '\n')
    fout.close()
    return


def file2dict(filename):
    """
    Reads dictionary from a specified file
    :param filename: filename to read dictionary from
    :type filename: String
    :return: Dictionary read from file
    :type: Dictionary
    """
    newDict = {}
    fin = open(filename, 'r', encoding='utf-8')
    for line in fin.read().split('\n'):
        if line != '':
            name, identifier = line.split(' : ')
            # print(name.strip())
            newDict[name.strip()] = int(identifier.strip())
    fin.close()
    return newDict


def encodeInfo(l, typeInfo, dpath='.'):
    """
    Transforms a list of String into a list of int. The strings are the messages to encode (ex: name of a drug).
    If a dictionary already exist, the new messages are appended and the one in the dictionary are mapped to the existing value
    :param l: Information messages to encode into int
    :type l: List of String
    :param typeInfo: Name of dictionary to look into
    :type typeInfo: String
    :param dpath: Dictionary file path
    :type dpath: String
    :return: Encoded list of messages

    Important Note: Do not use in parallel processes because the reference dictionaries will be of different length and
     the entries inconsistent

    Example: Supposing that the file 'letters.dictionary' does not exist in current working directory.
    >>> l1 = ["a","b"]
    >>> l_out = encodeInfo(l1,"letters",".")
    >>> l_out
    [0,1]
    >>> l2 = ["b","c"]
    >>> l_enc = encodeInfo(l2,"letters",".")
    >>> l_enc
    [1,2]
    >>> l_dec = decodeInfo([0,2,1,0],"letters",".")
    >>> l_dec
    ["a","c","b","a"]
    """

    dfilename = os.path.join(dpath, "{}.dictionary".format(typeInfo))

    lock = FileLock(dfilename + '.lock')
    lock.acquire()

    encodedInfo = list()
    flag_dictionary_edition = False

    if os.path.exists(dfilename):  # Check existance of a dictionnary
        # If exist then load it from file
        mydictionary = file2dict(dfilename)

    else:  # Does not exist, build newone
        mydictionary = {str(x): i for i, x in enumerate(list(set(l)))}
        # Current dictionnary was empty and is now created -> toggle flag
        flag_dictionary_edition = True

    #  Perform the encoding of our initial list
    for i, x in enumerate(l):
        if not str(x) in mydictionary:  # Make sure to write only new strings to the dictionnary
            mydictionary.update({str(x): len(mydictionary)})
            flag_dictionary_edition = True
        encodedInfo.append(mydictionary[str(x)])

    # Write the potentially modified dictionary to file
    if flag_dictionary_edition:
        dict2file(mydictionary, dfilename)


    lock.release()

    # Return the encoded list
    return encodedInfo


def decodeInfo(l, typeInfo, dpath='.'):
    """
    Uses the dictionary in file `dpath/typeInfo.dictionary` to convert the list of int into a list of String
    :param l: int to map to String
    :param typeInfo: Name of dictionary to look into
    :param dpath: Dictionary files path
    :type l: List of int
    :type typeInfo: String
    :type dpath: String

    :return:  Result of mapping from int to String message
    :rtype: List of String

    Example:
    See encodeInfo
    """
    dictpath = os.path.join(dpath, "{}.dictionary".format(typeInfo))
    lock = FileLock(dictpath + '.lock')
    lock.acquire()


    lout = list()

    if os.path.exists(dictpath):
        lock = FileLock(dictpath + '.lock')
        lock.acquire()

        mydictionnary = file2dict(dictpath)

        lock.release()

    else:
        print("Could not find dictionary {}".format(dictpath))
        sys.exit(1)

    for i, x in enumerate(l):  # Iterate on our list to decode,
        # remember that the lists are binaries X[i]=1 means that the ith entry of the dictonnary must be returned
        lout.append(list(mydictionnary.keys())[list(mydictionnary.values())[x]])


    return lout

