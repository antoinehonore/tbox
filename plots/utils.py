import matplotlib.pyplot as plt
import numpy as np


def plot(*args, **kwargs):
    plt.figure()
    plt.plot(*args, **kwargs)
    plt.pause(0.0001)
    return


def stdplot(*args, x=None, axis=0, ynames=None, lineOpts=None, emode="std"):
    # parse kwargs
    if ynames is None:
        ynames = [str(i) for i in range(len(args))]
    else:
        assert(len(ynames) == len(args))

    if lineOpts is None:
        lineOpts = ['-' for _ in range(len(args))]
    else:
        assert(len(lineOpts) == len(args))


    if x is None:
        x = np.arange(args[0].shape[1 if axis == 0 else 0])

    plt.figure()
    for y, yname, lopts in zip(args, ynames, lineOpts):
        ymean = y.mean(axis=axis)
        yerr = y.std(axis=axis)

        if emode == "sem":
            yerr /= np.sqrt(y.shape[axis])

        plt.plot(x, ymean, lopts, label=yname)
        plt.fill_between(x, ymean-yerr, ymean+yerr, facecolor='k', alpha=0.3)

    plt.legend()


def stdbar(*args, x=None, colors=None, barnames=None, emode="std", stat_fun=np.mean):
    nbars = len(args)
    if barnames is None:
        barnames = [str(i) for i in range(nbars)]
    else:
        assert(len(barnames) == len(args))

    if colors is None:
        cmap = plt.get_cmap("jet")
        colors_ = [cmap(-i*cmap.N//nbars) for i in range(nbars)]

    if x is None:
        x = list(range(nbars))

    emode_ = np.sqrt(np.array(args[0].shape[0])) if emode == "sem" else 1

    yerr_ = [np.array(ydata).std()/emode_ for ydata in args]
    ystat_ = [stat_fun(np.array(ydata)) for ydata in args]

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax2 = ax1.twiny()
    for xx, ystat, yerr, c,ydata in zip(x, ystat_, yerr_, colors_,args):
        ax1.bar(xx, ystat, yerr=yerr, color=c, alpha=0.3)
        ax1.scatter([xx]*len(ydata), ydata, s=5, color='k')
    ax1.set_xticks(x)
    ax1.set_xticklabels([x for x in barnames], rotation=90)
    ax2.set_xlim(ax1.get_xlim())
    ax2.set_xticks(x)
    ax2.set_xticklabels([x for x in barnames])

    #plt.show()
    plt.pause(0.0001)



def scatter(*args, xy=False):
    plt.figure()
    for x in args:
        if xy:
            plt.scatter(x[0], x[1])
        else:
            plt.scatter(list(range(np.array(x).shape[0])), x)
    plt.pause(0.0001)
    return

