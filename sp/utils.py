"""
Created on 17 Aug 2018

@author: anthon
"""

import numpy as np


def movmean(xin, m):
    """
    Performs moving average on signal passed as argument. Pass signals column wise in a matrix to process multiple signals.
    :param xin: Input signals (columnwise for multiple signals)
    :type xin: numpy.ndarray or List
    :param m: width of moving window
    :type m: int
    :return: Filtered signals
    :rtype: numpy.ndarray
    """

    if type(xin) is list:
        x = np.array(xin).reshape(-1, 1)
    else:
        x = xin

    lx, N = x.shape

    y = np.array(x)

    for n in range(N):
        for i in range(lx):
            frame = x[max(0, i - int(round(m / 2))):min(lx - 1, i + int(round(m / 2))), n]
            y[i, n] = np.sum(frame / frame.shape[0])
    return y

