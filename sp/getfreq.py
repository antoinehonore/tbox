import numpy as np


def getfreq(x, fs=1):
    '''
    x: numpy.ndarray contains signals columnwise
    fs: sampling frequency
    Returns a 1x1 numpy.ndarray containing the frequency f with higher amplitude in Hz,
    f is such that 2/lx < f < fs/2
    '''


    lx, ns = x.shape
    lx_s = lx / fs

    nfft = 2 ** 16

    # Frequency axis
    xF = np.fft.fftfreq(nfft, 1 / fs)  # Hz #(0:nfft)*fs/nfft

    # Zero mean
    xn = x - np.mean(x, axis=0)

    f = np.empty(ns)

    for i in range(ns):
        # Windowing
        sigin = np.multiply(xn[:, i], np.hamming(lx))

        # Fast Fourier Transform
        F = 1 / nfft * np.absolute(np.fft.fft(sigin, n=nfft))
        # Power spectrum
        P = np.square(np.abs(F))[:int(nfft / 2)]
        xF = xF[:int(nfft / 2)]

        # Compute gradient
        dP = np.gradient(P)
        # find local maxima
        peakidx = 1 + np.argwhere(np.logical_and(dP[0:dP.shape[0] - 1] > 0, dP[1:] < 0)).squeeze()

        # Apply low bound restriction
        sel_crit = xF[peakidx] > 5 / lx_s
        peakidx = peakidx[sel_crit]

        # All the Valid local maxima
        maxF = xF[peakidx]
        peakP = P[peakidx]

        # Find the highest peak
        try:
            maxpeakP = np.argmax(peakP)
            f[i] = maxF[maxpeakP]
        except:
            f[i] = 0
    return f  #  result in Hz

