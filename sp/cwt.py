
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt


def cwt(X, wavelet, widths):
    lx, ns = X.shape
    out = np.empty(ns)
    for i in range(ns):
        cwtout = signal.cwt(X[:, i], wavelet, widths)
        out[i] = np.argmax(np.sum(np.square(np.abs(cwtout)), axis=1))
    return out
