"""
Created on 06 Aug 2018

@author: anthon
"""

import numpy as np
#import cvxpy as cp
from scipy.linalg import circulant
from functools import partial
from tbox.plots.utils import plot
import matplotlib.pyplot as plt


class rconv(object):
    def __init__(self, d=100, n=2, verbose=True):
        self.n = n
        self.verbose = verbose
        self.H = [np.random.randn(d)/np.sqrt(d) for _ in range(self.n)]

    def fit(self, X):
        fp = np.vectorize(self._fit, signature="(n)->(k)")
        return fp(X)

    def _fit(self, x):
        f = partial(self.__fit, x=x)
        fp = np.vectorize(f, signature="(k)->(n)")
        out = fp(self.H)
        return out

    def __fit(self, h, x=None):
        h_mat = circ(h, x.shape[0])
        return x.reshape(1, -1) @ np.linalg.inv(h_mat)


def circ(h, n):
    return circulant(np.concatenate((h, np.zeros(n-h.shape[0]))))


if __name__ == "__main__":
    C = rconv()
    l = 1000
    X = np.random.rand(2, l)
    f = 2
    fe = 200
    t = np.arange(l)/fe
    X[0, :] = np.sin(2*np.pi*f*t)
    o = C.fit(X)
    print('')
