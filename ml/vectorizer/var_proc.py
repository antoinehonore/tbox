"""
Created on 14 Aug 2018

@author: anthon
"""

import numpy as np
from functools import partial
from sklearn.linear_model import Lasso as lasso
import warnings

# normalizer of color bar
from matplotlib.colors import Normalize

class MidpointNormalize(Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))

norm = MidpointNormalize(midpoint=0)


class var_proc(object):
    """
    Fit VAR processes and use the filters parameters as a vector representation of a multivariate time series.

    Parameters
    ----------
    p : tuple, optional, default range(4, 40, 5)
    Order of the filters to estimate. default: range(4, 40, step=5)

    Attributes
    ----------
    cumulative_sum : list
    Cumulative sum of the filters order.
    Defines the location of the i-th filter coefficients in the vectorized representation.
    """

    def __init__(self, p=tuple([i for i in range(4, 40, 5)]), lam=0, cv_perc=0):
        self.p = p
        self.mse = [[] for _ in range(len(self.p))]
        self.mse_cv = [[] for _ in range(len(self.p))]
        self.cond = [[] for _ in range(len(self.p))]
        self.lam = lam
        self.cv_perc = cv_perc
        pass


    def fit(self, X):
        """
        The Vector AR problem is formulated in the following way:
        x_t = [phi^0, phi^1, phi^2, ..., phi^p] * [1, x_{t-1}^T, x_{t-2}^T, ..., x_{t-p}^T]^T
        where
            n is the dimension of the time series, T the length
            for all t in {1,..., T}  x_t             is of size (n, 1)
            phi^p denotes the p-th order coef matrix is of size (n x n)

        Parameters
        ----------
        X : list of size m, composed of numpy.ndarray of size (n, T)
        List of n-dimensional timeseries to vectorize.

        Returns
        -------
        w : list of size m, composed of numpy.ndarray of size sum_{p \in self.p} m * (m * p + 1)
        Vectorized timeseries
        """
        f = np.vectorize(self.compute_, signature="(n,m)->(k)")
        w = f(X)
        return w


    def compute_(self, x):
        fp = partial(self.solver_, x=x)
        w = np.concatenate(list(map(fp, np.array(self.p))), axis=0).reshape(-1)
        assert(w.shape[0] == sum([x.shape[0] * (x.shape[0] * p + 1) for p in self.p]))
        return w


    def mse_(self, A, w, b):
        return ((b - (A @ w)) ** 2).sum() / b.shape[1] / b.shape[0]


    def solver_(self, p, x=None):
        """
        See https://en.wikipedia.org/wiki/General_matrix_notation_of_a_VAR(p)
        """

        mse_ = self.mse_

        d, T = x.shape
        Y = x[:, p:]

        #  Indep variable.
        Z = np.array([x[:, np.arange(i-1, i-1-p, -1)].reshape(-1, 1) for i in range(p, T)]).squeeze().T

        #  Intercept term.
        Z = np.vstack((np.ones(Z.shape[1]).reshape(1, -1), Z))

        #  Problem to solve -> Y = w Z <=> Z^T w^T = Y^T
        #  Solve.
        A = Z.T
        b = Y.T

        if self.cv_perc > 0:
            N = int(np.floor(A.shape[0] / d))
            ncv = int(np.floor(self.cv_perc * N))
            idx_cv_ = np.random.permutation(N)[:ncv]
            idx_cv = np.array([np.arange(x*d, x*d+d) for x in idx_cv_]).reshape(-1)

            Acv = np.take(A, idx_cv, axis=0)
            bcv = np.take(b, idx_cv, axis=0)

            A = np.delete(A, idx_cv, axis=0)
            b = np.delete(b, idx_cv, axis=0)

        if self.lam > 0:  # Thikonov regularization, inv solver: Ax = Id  -> LU factorization of A
            l = lasso(alpha=self.lam, fit_intercept=True, max_iter=2000)
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                l.fit(A[:, 1:], b)  # Remove intercep column in A

            w = np.concatenate((l.intercept_.reshape(1, -1), l.coef_.T), axis=0)

        else:   #   pinv solver:   SVD of A
            w = np.linalg.pinv(A) @ b

        if np.isnan(w).any():
            raise ValueError("NaN value in var_proc().fit().")

        # self.mse[self.p.index(p)].append(mse_(A, w, b))
        if self.cv_perc > 0:
            self.mse_cv[self.p.index(p)].append(mse_(Acv, w, bcv))

        #  self.cond[self.p.index(p)].append( np.linalg.norm(pZT, ord="fro") * np.linalg.norm(Z.T, ord="fro"))
        assert(w.shape == (d * p+1, d))
        return w.reshape(-1)


def var_proc_ptest(X, P, **kwargs):
    import matplotlib.pyplot as plt

    AR = var_proc(p=P)
    AR.fit(X)
    mse_ = np.array(AR.mse).mean(1)
    plt.figure()
    plt.plot(P, mse_)
    plt.pause(0.0001)
    return mse_


def test_var_proc():
    n, l = 10, 100
    #  x = np.arange(n*l).reshape(n, l)
    x = np.random.randn(n, l) + np.random.randn(10).reshape(-1, 1)*10
    AR = var_proc(lam=1, p=(10,))
    w, mse = AR.fit([x])
    print(mse)


if __name__ == "__main__":
    test_var_proc()
    print("")