"""
Created on 06 Aug 2018

@author: anthon
"""

import numpy as np


class ar_proc(object):
    """
    Fit AR processes and use the filters parameters as a vector representation of a time series.

    Parameters
    ----------
    p : tuple, optional, default range(4, 40, 5)
    Order of the filters to estimate. default: range(4, 40, step=5)

    Attributes
    ----------
    cumulative_sum : list
    Cumulative sum of the filters order.
    Defines the location of the i-th filter coefficients in the vectorized representation.
    """
    def __init__(self, p=tuple([i for i in range(4, 40, 5)])):
        self.p = p
        self.cumulative_sum = [0] + np.cumsum([i+1 for i in p]).tolist()
        pass

    def __call__(self, X):
        w = [np.zeros(self.cumulative_sum[-1]) for _ in range(len(X))]
        for j, x in enumerate(X):
            l = x.shape[0]
            for ip, p in enumerate(self.p):
                b = x[p:,:]
                A = np.array([x[np.arange(i-1, i-1 - p, -1), :] for i in range(p, l)])

                # Intercept term.
                A = np.hstack((A, np.ones(A.shape[0]).reshape(-1, 1)))

                w[j][self.cumulative_sum[ip]:self.cumulative_sum[ip]+p+1] = np.linalg.lstsq(A, b)[0]
                #print(w[j][:self.cumulative_sum[ip]+p+1])
        return w


if __name__ == "__main__":
    pass