'''
  Created by Laurens van der Maaten on 20-12-08.
  Copyright (c) 2008 Tilburg University. All rights reserved.
'''

import numpy as np


def pca(X=np.array([]), no_dims=50):
    """Runs PCA on the NxD array X in order to reduce its dimensionality to no_dims dimensions."""
    (n, d) = X.shape
    X = (X - np.tile(np.mean(X, 0), (n, 1))) / np.tile(np.std(X, 0), (n, 1))
    (l, M) = np.linalg.eig(np.dot(X.T, X))

    l = np.real(l)
    M = np.real(M)

    Y = np.dot(X, M[:, 0:no_dims])
    return Y, l, M
