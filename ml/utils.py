"""
Created on 20 Mar 2018

@author: anthon
"""

import numpy as np
import sys
from sklearn.preprocessing import OneHotEncoder


def to_onehot(Y):
    enc = OneHotEncoder(handle_unknown='ignore')
    enc.fit(np.array([0, 1]).reshape(-1, 1))
    Y_ = enc.transform(Y[:, np.newaxis]).toarray()
    return Y_


def cont_to_binary(y):
    return to_onehot(np.argmax(y, axis=1))

def test_cont_to_binary():
    y = np.array([[-1, -2],[-1000, -999], [0.3, 0.7]])
    assert ((cont_to_binary(y) == np.array([[1,0],[0,1],[0,1]])).all())


def div_dataset(X, T, test_perc=.2, saxis=0, dtype=np.float64):
    N = X.shape[saxis]
    idx = np.random.permutation(N)
    n2 = int(round(test_perc * N))

    X_ = np.take(X, idx[n2:], axis=saxis).astype(dtype=dtype)
    T_ = np.take(T, idx[n2:], axis=saxis).astype(dtype=dtype)
    Xt = np.take(X, idx[:n2], axis=saxis).astype(dtype=dtype)
    Tt = np.take(T, idx[:n2], axis=saxis).astype(dtype=dtype)

    return X_, T_, Xt, Tt


def res_str(d, signi=3):
    out_str = ""
    for k in d.keys():
        out_str += k + ": " + str(round(d[k], ndigits=signi)) + ", "
    out_str = out_str[:-2]
    #print(out_str)
    return out_str


def get_validation_data(X, T, xval=None, tval=None, perc=0.2):
    """
    Divide set into training and validation set. if xval or tval are already sets then the function is ignored and the inputs are returned
    :param X: Feature matrix
    :type X: ndarray
    :param T: Target matrix
    :type T: ndarray
    :param xval: if xval is None and tval is None, validation data is perc % of the training set
    :type xval: None or ndarray
    :param tval: if xval is None and tval is None, validation data is perc % of the training set
    :type tval: None or ndarray
    :param perc: percentage of training set to use as validation set
    :type perc: float between 0 and 1
    :return: Training features, training labels, validation features, validation labels.
    :rtype: List of ndarray
    """

    nfold = int(np.round(1/perc))
    if xval is None and tval is None:
        xfold, tfold, _ = get_kfold(X, T, nfold)
        Xout, Tout, Xval, Tval = isolate_fold(xfold, tfold, int(np.random.random_integers(0, nfold, 1)))
    else:
        Xout, Tout, Xval, Tval = X, T, xval, tval
    return Xout, Tout, Xval, Tval


def isolate_fold(xfolds, tfolds, k, samples_axis=1):
    """
    Concatenate folds and leave fold number k out,
    :param xfolds: List of feature folds (see get_kfold)
    :param tfolds: List of label folds (see get_kfold)
    :param k: index of the fold to leave apart
    :return: two first matrices are the concatenation of all folds but k-th fold. Two last are the k-th fold
    """

    # Check Inputs consistency

    assert (k < len(xfolds) and len(xfolds) == len(tfolds))



    tmpl = [x for i, x in enumerate(tfolds) if i != k]
    T = np.concatenate((tmpl), axis=samples_axis)
    Tout = tfolds[k]

    X = np.concatenate(([x for i, x in enumerate(xfolds) if i != k]), axis=samples_axis)
    Xout = xfolds[k]
    return X, T, Xout, Tout


def makeOnehot(ind):
    """
    Cast integer vector in one-hot matrix
    :param ind: 1d int vector of length n
    :return: row-onehot matrix
    :rtype: ndarray
    """
    if not ((np.round(ind) == ind).all()):
        print("Not all indices are integers")
        return -1

    Q = int(np.max(ind))
    n = ind.shape[0]
    B = np.zeros((n, Q))

    B[np.arange(n), ind-1] = 1
    return B


def mean_pm_std(x, signi=3):
    """
    Compute average and standard deviation of the values in x and write `mean +- std` into a string
    :param x: List of values
    :type x: List of numbers or 1d numpy arrays
    :param signi: number of significative digits
    :type signi: int
    :return: string containing mean +- std of x
    :rtype: String
    """
    return str(np.round(np.mean(x), signi)) + " +- " + str(np.round(np.std(x), signi))


def compute_accuracy(t, that):
    """
    Compute accuracy given that t is the target array
    :param t: one_hot QxN matrices, Q is the number of classes, N the number of samples
    :param that: QxN matrix, Q is the number of classes, N the number of samples
    :returns: Accuracy between 0 and 1
    """
    try:
        assert(t.shape == that.shape)
    except AssertionError:
        print("Shape inconsistency:", t.shape, that.shape)
        sys.exit(1)
    return np.sum((np.argmax(t, axis=0) - np.argmax(that, axis=0)) == 0) / t.shape[1]


def compute_nme(t, that):
    """
    Compute the Normalized Mean Error
    Parameters
    ----------
    t : Array like, size (n_targets, n_samples)
    True target value

    that : Array like, size (n_targets, n_samples)
    Predicted target value

    Returns
    -------
    Normalized Mean Error
    """
    try:
        assert(t.shape == that.shape)
    except AssertionError:
        print("Shape inconsistency:", t.shape, that.shape)
        sys.exit(1)
    return 20 * np.log10(np.linalg.norm(t - that, 'fro') / np.linalg.norm(t, 'fro'))


def compute_mse(t, that):
    """
    Compute Mean Squared Error
    Parameters
    ----------
    t : Array like, size (n_targets, n_samples)
    True target value

    that : Array like, size (n_targets, n_samples)
    Predicted target value

    Returns
    -------
    Mean Squared Error
    """

    try:
        assert(t.shape == that.shape)
    except AssertionError:
        print("Shape inconsistency:", t.shape, that.shape)
        sys.exit(1)
    return np.sum(np.square(t - that)) / t.shape[1]


def get_kfold(X, T, K, feat_axis=0):
    """
    Divide X and T into K folds
    :param X: Feature array (size d x N)
    :type X: ndarray
    :param T: Target array (size Q x N)
    :type T: ndarray
    :param K: Number of folds to create
    :return: List of K disjoint folds for features, List of K disjoint folds for targets, index of features into input matrices
    :rtype: List of ndarrays
    """


    sample_axis = 1 - feat_axis
    #idx = np.random.permutation(X.shape[0])
    #X = np.take(X, idx, axis=sample_axis)
    #T = np.take(T, idx, axis=sample_axis)

    # Check inputs consistency
    assert (X.shape[sample_axis] == T.shape[sample_axis])

    xfolds = np.array_split(X, K, axis=sample_axis)
    tfolds = np.array_split(T, K, axis=sample_axis)
    index_folds = np.array_split(np.arange(X.shape[sample_axis]), K)

    return xfolds, tfolds, index_folds


def reLu(x):
    '''
    Rectified Linear unit, map negative values to 0
    :param x: Input matrix
    :type x: ndarray
    :return: Rectified matrix
    :rtype: ndarray
    '''
    return np.maximum(0, x)


def softmax(x):
    '''
    Softmax operator (columnwise)
    :param x: Input matrix
    :type x: ndarray
    :return: Y such that \forall i sum( exp(Y[:,i]) ) = 1
    :rtype: ndarray
    '''
    return np.exp(x) / np.sum(np.exp(x), axis=0)


def normc(x, axis=0, out=None):
    '''
    Normalize the square of columns to 1, keeps the sign
    :param x: Input array to normalize
    :type x: ndarray
    :return: Normalized array
    :rtype: ndarray
    '''
    x_2 = np.square(x)
    return np.multiply(np.sqrt(np.divide(x_2, np.sum(x_2, axis=axis).reshape(-1, 1)), out=x_2), np.sign(x), out=out)

