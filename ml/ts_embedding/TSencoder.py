'''
Created on 16 May 2018

@author: anthon
'''
import numpy as np
from ..utils import reLu


class RNN_encoder(object):
    def __init__(self,input_size,hidden_size ):
        self.hidden_size=hidden_size
        self.input_size=input_size;
        self.V,_=   np.linalg.qr(np.random.randn(hidden_size,hidden_size));
        self.U=     np.random.randn(hidden_size,input_size)
        self.b=     np.random.randn(hidden_size,1)
    
    
    def encode(self,x):
        '''
        x is a d,lx time series, d in the dimension and lx the length
        '''
        
        d,lx=x.shape
        #print(d,lx)
        h=np.zeros((self.hidden_size,1))
        
        for i in range(lx):
            h= reLu(np.matmul(self.U,x[:,i]).reshape(-1,1) + self.b) + np.matmul(self.V,h) ;
        return h.reshape(-1);