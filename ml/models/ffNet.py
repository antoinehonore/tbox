'''
Created on 16 May 2018

@author: anthon
Implement feedforward network on top of torch implementation.

'''

import torch
import torch.nn as nn
import matplotlib.pyplot as plt

import torch.optim
from tbox.ml.utils import get_kfold
from tbox.utils import percprint
import copy, gc



class ffNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_output, nepoch, n_check, reg, stop_cond="convergence",verbose=0):
        '''
        :param input_size: Dimension of input features
        :param hidden_size: List of hidden nodes number in each hidden layer
        :param num_output: Dimension of output target
        :param nepoch: Maximum number of epoch
        :param n_check: Number of validation checks before early stoping condition
        :param reg: Weight decay parameter
        :param stop_cond: "convergence" (iterate Until training error increase during n_check consecutive iterations ) or
                            "nval"(iterate Until validation error increase during n_check consecutive iterations )
        :param verbose: verbosity level 0-> no writing on the stdout 1-> write performance of each iteration
        '''
        super(ffNet, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.nlayers = len(self.hidden_size)
        self.num_output = num_output
        self.nepoch = nepoch
        self.n_check = n_check
        self.stop_cond = stop_cond
        self.LAMBDA = reg

        if type(reg) is list:
            lam = reg[0]
        else:
            lam = reg

        self.fcin = nn.Linear(input_size, hidden_size[0])
        self.trin = nn.Tanh()

        [self.__setattr__("fc_" + str(i + 1), nn.Linear(self.hidden_size[i], self.hidden_size[i + 1])) for i in
         range(self.nlayers - 1)]
        [self.__setattr__("tr_" + str(i + 1), nn.ReLU()) for i in range(self.nlayers - 1)]

        self.fcout = nn.Linear(hidden_size[-1], num_output)

        self.criterion = nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.parameters(), lr=0.01, weight_decay=lam)

        self.name = "ffNet " + str(len(self.hidden_size)) + "l, " + str(["%03d n" % self.hidden_size[0]])
        self.verbose = verbose


    def reset(self):
        self.__init__(self.input_size, self.hidden_size, self.num_output, self.nepoch, self.n_check, self.LAMBDA)
        gc.collect()


    def forward(self, x):  # Forward pass: stacking each layer together
        out = self.fcin(x)
        out = self.trin(out)
        for fc, tr in zip([self.__getattr__("fc_" + str(i + 1)) for i in range(self.nlayers - 1)],
                          [self.__getattr__("tr_" + str(i + 1)) for i in range(self.nlayers - 1)]):
            out = fc(out)
            out = tr(out)
        # out = self.fc3(out)
        # out = self.transfer3(out)
        out = self.fcout(out)
        return out


    def stop_condition(self, train_err, val_err, best_params, best_perf, ibest):

        stop_training = False
        # best_model=self;

        if self.stop_cond == "nval":
            err_series = val_err

        elif self.stop_cond == "convergence":
            err_series = train_err

        niter = len(err_series)

        if niter == 1:
            best_perf = err_series[-1]
            best_params = copy.deepcopy(self.state_dict())
            ibest = 1

        else:
            if niter - ibest > self.n_check:
                self.load_state_dict(best_params)
                stop_training = True

            else:
                if err_series[-1] < best_perf:
                    # best_model=torch.serialize(self)
                    best_params = copy.deepcopy(self.state_dict())  # .copy()
                    # torch.save(self.state_dict(), 'BestSoFar.so')
                    best_perf = err_series[-1]
                    ibest = niter

        return stop_training, best_params, best_perf, ibest


    def train(self, Xin, Tin, Xtin, Ttin, xval=None, tval=None, verbose=0):
        if xval is None and tval is None:
            xfold, tfold, _ = get_kfold(Xin, Tin, 8)
            X, T, Xval, Tval = get_train_cv_sets(xfold, tfold, 3)
            X, T, Xval, Tval = makeVariable(X, T, Xval, Tval, flag=1)
            validationset = True
        elif xval == [] and tval == []:
            validationset = False
            X, T = makeVariable(Xin, Tin, flag=1)
        else:
            X, T, Xval, Tval = makeVariable(Xin, Tin, xval, tval, flag=1)
            validationset = True

        Xt, Tt = makeVariable(Xtin, Ttin, flag=1)

        nepoch = self.nepoch

        train_mse = []
        test_mse = []
        val_mse = []
        best_epoch = 0
        best_perf = 0
        ibest = 0
        best_params = copy.deepcopy(self.state_dict())
        val_loss = 0

        for epoch in range(nepoch):
            # Train run
            _, _ = self.run_epoch(X, T)
            train_loss, _ = self.forward_batch(X, T)
            train_mse.append(train_loss)
            if validationset:
                # Validation run
                val_loss, _ = self.forward_batch(Xval, Tval)
                val_mse.append(val_loss)

            # Test run
            test_loss, Tthat = self.forward_batch(Xt, Tt)
            test_mse.append(test_loss)

            # self.stop_condition(train_mse)
            stop_training, best_params, best_perf, ibest = self.stop_condition(train_mse, val_mse, best_params,
                                                                               best_perf, ibest)
            if stop_training:
                break
            if validationset:
                if verbose > 0:
                    print("Epoch: ", epoch, "Train loss: ", train_loss, "Val loss: ", val_loss, "Test loss: ", test_loss, '<-' if ibest-1 == epoch else '')


        xtmp, ttmp = makeVariable(Xin, Tin)
        _, T_hat = self.forward_batch(xtmp, ttmp)
        _, best_testhat = self.forward_batch(Xt, Tt)

        results = {"T_hat": T_hat.data.numpy(), "Tt_hat": best_testhat.data.numpy(), "extra": []}  # val_mse[best_epoch]
        return results


    def run_epoch(self, X, T):
        '''
        :param X: Input features, ndarray of size (D,N)
        :param T: Input labels,  ndarray of size (Q,N)
        :return:  Run one epoch, backpropagate loss after each training example
                    return the average loss and prediction
        '''
        d, ntrain = X.data.shape
        epoch_loss = 0
        That = T.clone()

        # self.optimizer.zero_grad();
        for isample in range(ntrain):
            self.optimizer.zero_grad()
            that = self.forward(X[:, isample])
            That[:, isample].data = that.data

            loss = self.criterion(that, T[:, isample])
            loss.backward()
            epoch_loss += loss.data[0]
            # if divmod(isample,32)[1] == 0:
            self.optimizer.step()


        return round(epoch_loss / ntrain, 2), That


    def forward_batch(self, X, T):
        '''
        :param X: Input features, ndarray of size (D,N)
        :param T: Input labels,  ndarray of size (Q,N)
        :return:  Perform the forward pass of a batch of feature and returns the averaged performance, prediction
        '''
        That = Variable(torch.zeros(T.data.shape), requires_grad=False)
        total_loss = 0

        for i in range(X.data.shape[1]):
            that = self.forward(X[:, i])
            loss = self.criterion(that, T[:, i])
            That.data[:, i] = that.data
            total_loss += loss.data[0]

        return round(total_loss / X.data.shape[1], 2), That


    def learningCurve(self, X, T, xval=None, tval=None, nmax=300):
        xtrain, ttrain, Xval, Tval = get_validation_data(X, T, xval, tval)

        m = xtrain.shape[1]
        nmodel = len(self.LAMBDA)

        xx = list(range(2, m + 1, 10))
        error_train = np.empty((len(xx), nmodel))
        error_val = np.empty((len(xx), nmodel))


        for ilambda in range(nmodel):
            plt.figure(ilambda)
            for k, i in enumerate(xx):
                percprint(k, len(xx))
                self.reset()
                self.optimizer = torch.optim.Adam(self.parameters(), lr=0.01, weight_decay=self.LAMBDA[ilambda])
                self.train(xtrain[:, :i], ttrain[:, :i], Xval, Tval, xval=[], tval=[])

                xtrainVar, TtrainVar, XvalVar, TvalVar = makeVariable(xtrain[:, :i], ttrain[:, :i], Xval, Tval, flag=1)

                error_train[k, ilambda], ttrainhat = self.forward_batch(xtrainVar, TtrainVar)
                error_val[k, ilambda], Tvalhat = self.forward_batch(XvalVar, TvalVar)

                print("#samples:", i, "Train err:", error_train[k, ilambda], "Val err:", error_val[k, ilambda])

                # Plot
                plt.figure(ilambda)
                plt.cla()
                plt.plot(xx[:k + 1], error_train[:k + 1, ilambda], label="Train error")
                plt.plot(xx[:k + 1], error_val[:k + 1, ilambda], label="Val error")
                plt.title("Learning Curves, model: " + self.name + " lambda:" + str(self.LAMBDA[ilambda]))
                plt.legend()
                plt.pause(0.0001)
            plt.savefig("LC_" + self.name + " lambda:" + str(self.LAMBDA[ilambda]) + '.jpg')
        if nmodel > 1:
            plt.figure()
            plt.plot(np.log10(self.LAMBDA), error_train[-1, :], label="Train error")
            plt.plot(np.log10(self.LAMBDA), error_val[-1, :], label="Val error")
            plt.xlabel("log10-Lambda")
            plt.title("Val error vs lambda, model: " + self.name)
            plt.savefig("regCurve_" + self.name + " lambda:" + str(self.LAMBDA[ilambda]) + '.jpg')
        return error_train, error_val

