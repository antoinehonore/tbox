
__all__ = ["DKN",
           "DKF",
           "ELM",
           "ffNet",
           "Hybrid",
           "myLSTM",
           "PLN",
           "RLS",
           "MLP"]

