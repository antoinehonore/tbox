"""
Created on 16 May 2018

@author: anthon
"""

import numpy as np
import sklearn
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression


class ELM(sklearn.base.BaseEstimator):
    """
    Extreme Learning Machine.

    Parameters
    ----------
    metric : Callable, optional, default: r2_score
    Function to score prediction.

    alphas : tuple, optional, default: 10**-4...10**4
    Regularization parameters

    K : int, optional, default: 3
    Number of fold in cross-validation

    n : int, optional, default: 1000
    Number of random nodes in input layer.
    """

    def __init__(self, metric=accuracy_score, alpha=1,\
                 val_idx=None, K=3, nnodes=1000, max_iter=10000):
        super(ELM, self).__init__()
        self.alpha = alpha
        self.K = K
        self.nnodes = nnodes
        self.metric = metric
        self.R = None
        self.map_to_target = None
        self.val_idx=val_idx
        self.max_iter = max_iter
        self.status = "Initialized"
        self.name = "ELM"

    def get_params(self, deep=True):
        return {k: self.__getattribute__(k) for k in ["nnodes", "alpha"]}

    def set_params(self, **params):
        for k, v in params.items():
            self.__setattr__(k, v)
        return self

    def reset(self):
        self.__init__(alpha=self.alpha, K=self.K, nnodes=self.nnodes)


    def nlinearity(self, x):
        return np.maximum(0, x)


    def fit(self, X, T_):
        if self.status == "Fitted":
            self.reset()
        T = np.argmax(T_, axis=1)

        N, d = X.shape
        self.R = np.random.randn(d, self.nnodes)
        self.B = np.random.randn(1, self.nnodes)
        Z = X @ self.R + self.B
        Y = self.nlinearity(Z)

        rls = LogisticRegression(C=1./self.alpha,multi_class='multinomial',solver='lbfgs',max_iter=self.max_iter)
        rls.fit(Y, T)
        self.map_to_target = rls

        self.status = "Fitted"
        return self


    def predict(self, X):
        """
        Predict targets from feature vectors.

        Parameters
        ----------
        X : numpy.ndarray, size (n_samples, n_features)
        Feature vector to predict targets of.

        Returns
        -------
        that : numpy.ndarray, size (n_samples, n_targets)
        Matrix of predicted targets.
        """
        Y = self.nlinearity(X @ self.R + self.B)
        that = self.map_to_target.predict(Y)
        return that

    def predict_proba(self, X):
        """
        Predict targets probability from feature vectors.

        Parameters
        ----------
        X : numpy.ndarray, size (n_samples, n_features)
        Feature vector to predict targets of.

        Returns
        -------
        that : numpy.ndarray, size (n_samples, n_targets)
        Matrix of predicted targets.
        """
        Y = self.nlinearity(X @ self.R + self.B)
        that = self.map_to_target.predict_proba(Y)
        return that # np.exp(that) / np.sum(np.exp(that), axis=1)[:, np.newaxis]


    def score(self, X, T):
        """
        Scores feature vectors prediction vs. feature vector target

        Parameters
        ----------
        X : numpy.ndarray, size (n_samples, n_features)
        Feature vector to score the predict targets of.

        T : numpy.ndarray, size (n_samples, n_targets)
        Target vectors associated with x.

        Returns
        -------
        scores : numpy.ndarray (n_samples,)
        Scoring of the predictions of input feature vectors.
        """
        that = np.argmax(self.predict_proba(X), axis=1)
        score = self.metric(np.argmax(T, axis=1), that)
        return score
