"""
Created on 16 May 2018

@author: anthon
"""


import numpy as np
from tbox.ml.utils import reLu, isolate_fold, get_kfold, normc
import matplotlib.pyplot as plt
from multiprocessing import Pool

import sklearn
from sklearn.metrics.scorer import r2_score

from tbox.ml.models.utils import alpha2str, mathlatex
from tbox.ml.utils import div_dataset

from tbox.optim import admm_solver
import scipy
from scipy.spatial.distance import pdist, squareform
from tbox.ml.utils import compute_accuracy
from tbox.ml.models.utils import rbf_


class DKN(object):
    """
    Dense Kernel Network.

    Parameters
    ----------
    nnodes : int, optional, default: 1000
    Number of random nodes at each layer.

    lmax : int, optional, default: 10
    Maximum number of layers.

    alphas : tuple, optional, default: 10^-2..10^3
    Regularization parameters of the kernel regressions.

    K : int, optional, default: 3
    Number of folds used for cross-validation.
    If K == 1 : The cross-validation is skipped and the first element of the alphas tuple is used at each layer.

    metric : Callable, optional, default: sklearn.metric.scorer.r2_score
    Scoring function.

    Attributes
    ----------
    map_to_target : list of numpy.ndarray
    Matrices used for mapping to target space.

    fulltrain_kern : list of numpy.ndarray
    Training data kernel at each layer.

    input_fulltrain : list of numpy.ndarray
    Store the training data input at each layer. This is to be able to compute the data kernel at prediction time.

    total_number_layer : int
    Increase by 1 each time a layer is added to the network.

    userdata : dictionary
    Stores information about the model: name, architecture, regression parameter.

    status : str
        'Initialized' : Is set after initialization
        'Converged' : Is set after fitting is done. If status == 'Converged' and the method fit() is called, the model
            is reinitialized before fitting.

    R1 : numpy.ndarray, size (d_features, nnodes)
    Input layer random matrix.

    R : list, length lmax - 1
    Hidden layers random matrix.

    VQ : numpy.ndarray, size (2 * n_targets, n_targets)
    Target estimator transformation, inherited from PLN.

    """

    def __init__(self, alphas=tuple([10**i for i in range(-5, 6)]), metric=r2_score, nnodes=1000,\
                 lmax=10, K=3):
        self.nnodes = nnodes
        self.lmax = lmax
        self.alphas = alphas
        self.K = K
        self.metric = metric

        #Keep data in memory for prediction
        self.map_to_target = [0 for _ in range(lmax)]
        self.fulltrain_kern = [0 for _ in range(lmax)]
        self.input_fulltrain = [0 for _ in range(lmax)]

        self.train_data = None
        self.train_label = None
        self.train_perf = [0 for _ in range(self.lmax)]

        self.A = [None for _ in range(self.lmax)]
        self.y = [None for _ in range(self.lmax)]
        self.K = [None for _ in range(self.lmax)]
        self.tval_hat = np.zeros((self.lmax, len(self.alphas)))


        self.total_number_layers = 0

        self.userdata = {"name": "DKN", "arch": str((nnodes, lmax))}
        if K > 1:
            self.userdata["reg"] = mathlatex(alpha2str(self.alphas[0])+"\dots"+alpha2str(self.alphas[-1]))
        else:
            self.userdata["reg"] = mathlatex(alpha2str(self.alphas[0]))
            self.userdata["name"] += "2"

        self.status = "Initialized"


    def reset(self):
        """Reset the object random matrices, kernels, inputs and maps list."""
        self.__init__(alphas=self.alphas, metric=self.metric, nnodes=self.nnodes, lmax=self.lmax, K=self.K)


    def fit(self, X, T):
        """
        Fit the model to the data in argument.

        Parameters
        ----------
        X : numpy.ndarray size (n_samples, n_features)
        Training data matrix.

        T : numpy.ndarray size (n_samples, n_targets)
        Training target matrix.

        """
        if self.status == "Converged":
            self.reset()


        N, Q = T.shape
        _, d = X.shape


        K = self.K
        nalphas = len(self.alphas)
        L = self.lmax

        #  Declare Random weights matrix
        self.R1 = np.random.randn(d, self.nnodes)# / np.sqrt(self.nnodes)
        self.R = np.random.randn(self.nnodes + 2 * Q, self.nnodes, L - 1)# / np.sqrt(self.nnodes)
        VQ = np.concatenate((np.eye(Q), -np.eye(Q)), axis=0)
        self.VQ = VQ

        #  Find constant features and correct them with additional very subtle white noise to avoid numerical issues
        #  idx = np.argwhere(X.std(0) == 0).reshape(-1)
        #  X[:, idx] = X[:, idx]  # + np.random.randn(X.shape[0], idx.shape[0])*(10**-5)
        if self.K > 1:
            xfolds, tfolds, idxfolds = get_kfold(X, T, self.K, feat_axis=1)
            input_fulltrain = np.concatenate(xfolds, axis=0)
            target_fulltrain = np.concatenate(tfolds, axis=0)

        else:
            xtrain, ttrain, xval, tval = div_dataset(X, T, test_perc=.2, saxis=0)

            input_fulltrain = xtrain
            target_fulltrain = ttrain


        Ktrain = [0 for _ in range(K)]
        Kval = [0 for _ in range(K)]
        K_fulltrain = 0

        input_ktrain = [0 for _ in range(K)]
        input_kval = [0 for _ in range(K)]
        that_kval = [[[] for _ in range(nalphas)] for _ in range(K)]
        that_ktrain = [[[] for _ in range(nalphas)] for _ in range(K)]

        val_score = np.empty([L, nalphas, K])

        best_alpha = np.zeros(L)
        best_val = np.zeros(L)

        l = 0
        self.K = 0

        while (self.status != "Converged") and l < L:
            if l == 0:
                R_ = self.R1

            else:
                R_ = self.R[:, :, l - 1]



            self.A[l] = np.random.randn(y_in.shape[1], self.nnodes + self.delta * l)
            self.y[l] = self.l_relu(self.normalize(y_in @ self.A[l]))
            self.K[l] = self.kernel(self.y[l], self.y[l]) + K_in

            if K == 0:
                best_alpha[l] = self.alphas[0]


            elif K > 0:

                #  Iterate over all folds
                for k in range(K):
                    #TODO possible to parallelize ? Probably won't work on MACOSX because of the BLAS lib used by numpy.
                    if K > 1:
                        _, target_ktrain, _, target_kval = isolate_fold(xfolds, tfolds, k, samples_axis=0)

                        if l == 0:
                            input_ktrain[k], _, input_kval[k], _ = isolate_fold(xfolds, tfolds, k, samples_axis=0)

                    elif K == 1:
                        if l == 0:
                            input_ktrain[k], target_ktrain, input_kval[k], target_kval = xtrain, ttrain, xval, tval

                    #  Compute train/val Kernel for the k-th fold
                    Ktrain[k] = self.update_kernel(input_ktrain[k], input_ktrain[k], Ktrain[k])
                    Kval[k] = self.update_kernel(input_ktrain[k], input_kval[k], Kval[k])

                    for il, alpha in enumerate(self.alphas):
                        #    TEMP = T[K+\alpha N Id]^-1
                        TEMP = self.solve_regression(Ktrain[k], target_ktrain, alpha)

                        that_ktrain[k][il] = (Ktrain[k].T @ TEMP)
                        that_kval[k][il] = (Kval[k].T @ TEMP)

                        val_score[l, il, k] = self.metric(target_kval, that_kval[k][il])


                #  Choose the best alphas wrt average validation error
                avg_val_score = val_score[l, :, :].mean(axis=1).reshape(-1)
                assert(avg_val_score.shape[0] == nalphas)

                ibest_alpha = np.argmax(avg_val_score)
                best_val[l] = avg_val_score[ibest_alpha]

                best_alpha[l] = self.alphas[ibest_alpha]


            if l == 0 or best_val[l] >= best_val[l - 1]:
                # Testing
                K_fulltrain = self.update_kernel(input_fulltrain, input_fulltrain, K_fulltrain)

                self.fulltrain_kern[l] = K_fulltrain
                self.input_fulltrain[l] = input_fulltrain

                #  print("l",l,"K:", (K_fulltrain).mean())
                TEMP = self.solve_regression(K_fulltrain, target_fulltrain, best_alpha[l])

                self.map_to_target[l] = TEMP
                that_fulltrain = (K_fulltrain.T @ self.map_to_target[l])
                if K > 0:
                    for k in range(K):
                        input_ktrain[k] = self.pln(input_ktrain[k], R_, that_ktrain[k][ibest_alpha], VQ)
                        input_kval[k] = self.pln(input_kval[k], R_, that_kval[k][ibest_alpha], VQ)

                input_fulltrain = self.pln(input_fulltrain, R_, that_fulltrain, VQ)

                l += 1

            else:
                self.status = "Converged"
        self.alpha = best_alpha[l-1]
        #   Write converged if the limit of layers in attained.
        self.status = "Converged"
        self.total_number_layers = l
        return self


    def solve_regression(self, K, T, alpha):
        n = K.shape[0]
        return np.linalg.inv(K + alpha * n * np.eye(n)) @ T


    def predict(self, x):
        """
        Predict targets from feature vectors.

        Parameters
        ----------
        x : numpy.ndarray, size (n_samples, n_features)
        Feature vector to predict targets of.

        Returns
        -------
        that : numpy.ndarray, size (n_samples, n_targets)
        Matrix of predicted targets.
        """

        data_kern = 0
        input_ = x
        for l in range(self.total_number_layers):
            if l == 0:
                R_ = self.R1
            else:
                R_ = self.R[:, :, l-1]

            data_kern = self.update_kernel(self.input_fulltrain[l], input_, data_kern)
            that = (data_kern.T @ self.map_to_target[l])

            input_ = self.pln(input_, R_, that, self.VQ)

        return that


    def score(self, x, t):
        """
        Scores feature vectors prediction vs. feature vector target

        Parameters
        ----------
        x : numpy.ndarray, size (n_samples, n_features)
        Feature vector to score the predict targets of.

        t : numpy.ndarray, size (n_samples, n_targets)
        Target vectors associated with x.

        Returns
        -------
        scores : numpy.ndarray (n_samples,)
        Scoring of the predictions of input feature vectors.
        """
        that = self.predict(x)
        return self.metric(t, that)


    def update_kernel(self, x, y, kernel_old):
        return rbf_(x, y, gamma=None) + kernel_old + x @ y.T
        #return sklearn.metrics.pairwise_kernels(x, y, metric="cosine") + kernel_old



    def gkernel(self, x, y):
        N1 = x.shape[1]
        N2 = y.shape[1]

        n1sq = np.sum(np.square(x), axis=0).reshape(1, N1)
        n2sq = np.sum(np.square(y), axis=0).reshape(1, N2)

        D = (np.ones((N1, 1)) @ n1sq).T + (np.ones((N1, 1)) @ n1sq).T - 2 * (x.T @ x)
        temp = 2 * np.sum(np.sum(D)) / N1 / N1
        del D

        D = (np.ones((N2, 1)) @ n1sq).T + (np.ones((N1, 1)) @ n2sq) - 2 * (x.T @ y)

        return np.exp(np.divide(-D, temp))


    def normalize(self, x):
        #return x
        return normc(x, axis=1)  #   np.multiply( np.sqrt(    np.divide( np.square(x) , np.sum(np.square(x),axis=0) )    ),  np.sign(x))


    def pln(self, x, R, that, VQ):
        Z = np.concatenate((that @ VQ.T, self.normalize(x @ R)), axis=1)
        Y = np.maximum(0, Z)# + .1 * np.minimum(0, Z)
        return Y

