'''
Created on 9 Jan 2018

@author: antoinehonore
'''
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import LSTM
from torch.autograd import Variable

import matplotlib.pyplot as plt
import numpy as np


class myLSTM(LSTM):
    '''
    classdocs
    '''


    def __init__(self, input_size, hidden_size, output_size, nlayers):
        '''
        Constructor
        '''
        
        super(LSTM,self).__init__('LSTM', input_size, hidden_size, nlayers)#,input_size, hidden_size, nlayers).__init__()
        self.hidden_size=       hidden_size
        self.nlayers=           nlayers
        self.output_size=       output_size
        self.hidden2target=     nn.Linear(hidden_size, output_size)
        
    def run_epoch(self,X,T,criterion,optimizer):
        N=X.size(2)
        epoch_loss=0;
        #print("epoch",epoch,"/",nepoch)
        That = T.clone();
        
        for isample in range(N):
            #forward pass
            that=   self.forward_full_ts(X[:,:,isample])
            That[0,isample]=that;
            
            optimizer.zero_grad()
            loss=   criterion(that,T[0,isample]);
            loss.backward();
            optimizer.step()
            epoch_loss+=loss.data[0];
        return np.round(epoch_loss/N,3),That;
    
    def run_batch(self,X,T,criterion):
        batch_loss=0;
        N=X.size(2);
        That=T.clone()
       
        for isample in range(N):
            that=   self.forward_full_ts(X[:,:,isample]);
            That[0,isample]=that;
            batch_loss+=criterion(that,T[0,isample]).data[0]
        return np.round(batch_loss/N,3),That
    
    def init_hidden(self,hidden_size,nlayers):
        return (Variable(torch.zeros(nlayers,1 , hidden_size)),                 Variable(torch.zeros(nlayers, 1, hidden_size)))
    
    def forward_full_ts(self, x):
        lx=         x.size(0)
        
        hidden=     self.init_hidden(self.hidden_size, self.nlayers)
        #print(x[0,:].contiguous().view(1,-1,1))
        for ii in range(lx):
            lstm_out, hidden = self.forward( x[ii,:].contiguous().view(1,1,-1), hidden )
            
        that=       self.hidden2target( hidden[0] )
        #print(that.shape,hidden[0].shape)
        return that,hidden
    
     
    def train(self, xtrain, ttrain, xtest, ttest, criterion, optimizer, niter):
        Loss=           []
        Acc=            []
        test_Acc=       []
        ntrain=         len(xtrain)
        ntest=          len(xtest)
        
        plt.figure()
        for epoch in range(niter):
            epoch_loss_value=           0
            epoch_acc_value=            0
            
            for i in range(ntrain):
                if i>0 and divmod( int(100*i/ntrain),25)[1]==0 and divmod( int(100*i/ntrain),25)[0]!=0:
                    print("epoch:",epoch,",",100*i/ntrain,"%, loss:",(epoch_loss_value.data[0]/i) if i>0 else 0)
                ts_x=   Variable(torch.FloatTensor(xtrain[i]))
                ts_t=   Variable(torch.FloatTensor(ttrain[i]))
                
                
                optimizer.zero_grad()
                
                #Forward pass
                that=   self.forward_full_ts(ts_x)
                
                that=   that[:,-1]
                
                loss=                   criterion( that, ts_t )#*Variable(torch.ones(1,1))
                loss.backward()
                optimizer.step()
                
                epoch_acc_value+=       iscorrect(ts_t.data.numpy(), that.data.numpy())
                epoch_loss_value+=      loss[0]
            
            #+run on test set+
            epoch_test_acc_value=0
            for itest in range(ntest):
                ts_x=   Variable(torch.FloatTensor(xtest[itest]))
                ts_t=   Variable(torch.FloatTensor(ttest[itest]))
                
                
                hidden= self.init_hidden(self.hidden_size,self.nlayers)
                for ii in range(ts_x.size()[1]):
                    inputs=ts_x[:,ii]*Variable(torch.FloatTensor((np.ones((1,1)))))
                    lstm_out, hidden = self.forward(inputs.view(1,1,-1), hidden)
                
                that=       self.smax( self.hidden2target( lstm_out.view(-1,self.hidden_size)) )
                
                epoch_test_acc_value += iscorrect(ts_t.data.numpy(), that.data.numpy())
                
            Loss.append(float(epoch_loss_value.data.numpy())/ntrain)
            Acc.append(float(epoch_acc_value)/ntrain)
            test_Acc.append(float(epoch_test_acc_value)/ntest)
            
            print("epoch:",epoch,"Loss: ",Loss[len(Loss)-1],", train acc=",Acc[len(Acc)-1],"%, test acc=",test_Acc[len(test_Acc)-1],"%")
            
            plt.cla()
            plt.plot(Acc, label='Training Accuracy')
            plt.plot(test_Acc, label='Testing Accuracy')
            plt.legend()
            plt.pause(.1)
        return Loss, Acc, test_Acc
    
def iscorrect(t,o):
    io=o.argmax()
    it=t.argmax()
    return (io==it)   
        #print(Loss)