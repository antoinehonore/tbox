"""
Created on 02 Oct 2018

@author: Antoine Honore, ahonore@pm.me
"""

import numpy as np
import sys

from sklearn.metrics.scorer import r2_score
import matplotlib.pyplot as plt

from tbox.ml.utils import div_dataset, normc
from tbox.ml.models.utils import rbf_



class DKF(object):
    def __init__(self, alphas=[10**i for i in range(-6, 4)], lmax=3, delta=50,\
                 nnodes=1000, metric=r2_score,val_idx=None):
        self.leaky_relu_coef = 0.01
        self.decay = .9
        self.delta = 50
        self.alphas = alphas
        self.lmax = lmax
        self.deta = delta
        self.nnodes = nnodes
        self.metric = metric
        self.gamma = [0 for _ in range(self.lmax)]
        self.val_idx = val_idx

        metric_str = str(metric)

        if "r2_score" in metric_str or "accuracy" in metric_str:
            self.choice_fun = np.argmax
        elif "nme" in metric_str:
            self.choice_fun = np.argmin
        else:
            print("unknown metric function. Exit.", metric)
            sys.exit(1)
        self.metric_name = metric_str.split(' ')[1]

        self.train_data = None
        self.train_label = None
        self.train_perf = [0 for _ in range(self.lmax)]

        self.A = [None for _ in range(self.lmax)]
        self.y = [None for _ in range(self.lmax)]
        self.K = [None for _ in range(self.lmax)]
        self.tval_hat = np.zeros((self.lmax, len(self.alphas)))
        self.name = "DKF"


    def improvement(self, old, new):
        if self.choice_fun == np.argmin:
            return new < old

        if self.choice_fun == np.argmax:
            return new > old

    def fit(self, X, T, vperc=.15, enplot=False):

        #  xtrain, ttrain, xval, tval = div_dataset(X, T, test_perc=vperc, saxis=0)
        N = X.shape[0]
        nval = int(round(N*vperc))
        permut = np.random.permutation(N)
        val_idx = permut[:nval]
        train_idx = permut[nval:]

        self.train_data = X
        self.train_label = T

        y_in = self.train_data
        K_in, gamma = self.kernel(self.train_data, self.train_data)
        #K_in = K_in + self.train_data @ self.train_data.T

        self.gamma_init = gamma

        #   Kval_in = self.kernel(self.train_data, xval)
        #   yval_in = xval


        self.val_perf = np.zeros(self.lmax)
        self.train_perf = np.zeros_like(self.val_perf)


        for l in range(self.lmax):
            layer_val_perf = np.zeros(len(self.alphas))
            layer_train_perf = np.zeros_like(layer_val_perf)


            self.K[l] = K_in


            # 1-fold cross validation.
            Kval = self.K[l][train_idx, :][:, val_idx]
            Ktrain = self.K[l][train_idx, :][:, train_idx]

            for ialpha, alpha in enumerate(self.alphas):
                tval_hat = Kval.T @ self.solve_regression(Ktrain, T[train_idx, :], alpha)
                ttrain_hat = Ktrain.T @ self.solve_regression(Ktrain, T[train_idx, :], alpha)
                layer_val_perf[ialpha] = self.metric(T[val_idx, :], tval_hat)
                layer_train_perf[ialpha] = self.metric(T[train_idx, :], ttrain_hat)


            ibest = self.choice_fun(layer_val_perf)
            self.val_perf[l] = layer_val_perf[ibest]
            self.alpha = self.alphas[ibest]
            self.train_perf[l] = self.metric(T, self.K[l].T @ self.solve_regression(self.K[l], T, self.alpha))

            #tmp = self.score(self.train_data, self.train_label)
            if l == 0 or self.improvement(self.val_perf[l-1], self.val_perf[l]):
                self.A[l] = np.random.randn(y_in.shape[1], self.nnodes + self.delta*l)


                y_in = self.l_relu(self.normalize(y_in @ self.A[l]))
                self.y[l] = np.copy(y_in)

                Ktmp, gamma = self.kernel(y_in, y_in)
                K_in = Ktmp + K_in# + y_in @ y_in.T

                self.gamma[l] = gamma

            else:
                self.y[l-1], self.A[l-1], self.K[l] = None, None, None
                break

        return self


    def predict(self, x):
        y_test = [None for _ in range(self.lmax)]
        y_in = x
        K_test = self.kernel(self.train_data, x, gamma=self.gamma_init)
        Lmax = -1

        for l in range(self.lmax):
            if self.A[l] is None:
                Lmax = max(0, l)
                break

            y_test[l] = self.l_relu(self.normalize(y_in @ self.A[l]))
            K_test = self.kernel(self.y[l], y_test[l], gamma=self.gamma[l]) + K_test
            y_in = y_test[l]

        #  return np.linalg.lstsq(y_in.T, self.train_label)[0].T @ y_in
        return K_test.T @ self.solve_regression(self.K[Lmax], self.train_label, self.alpha)


    def score(self, x, t):
        that = self.predict(x)
        return self.metric(t, that)

    #   Kernel computation
    def kernel(self, x, y, gamma=None):

        K, gamma_out = rbf_(x, y, gamma=gamma)

        if gamma is None:
            return K, gamma_out
        else:
            return K
        #  return sklearn.metrics.pairwise_kernels(x, y, metric="rbf")


    #  Regression
    def solve_regression(self, K, T, alpha):
        n = K.shape[0]
        return np.linalg.inv(K + alpha * n * np.eye(n)) @ T
        #return admm_solver((K + n * alpha * np.eye(n)), T.T, alpha, .01, 100).T

    def normalize(self, x):
        #return x
        return normc(x, axis=1)

    #  Leaky relu and inverse
    def l_relu(self, x):
        x[x < 0] = self.leaky_relu_coef*x[x < 0]
        return x

    def l_relu_inv(self, x):
        x[x < 0] = self.leaky_relu_coef**(-1) * x[x < 0]
        return x


    def forward(self, x):
        y_in = x
        for l in range(self.lmax):
            y = self.l_relu(self.normalize(y_in @ self.A[l]))
            y_in = y
        return y_in


    def invert(self, y):
        y_in = y

        for l in sorted(range(self.lmax), reverse=True):
            x = np.linalg.pinv(self.A[l]) @ self.l_relu_inv(y_in)
            y_in = x
        return x



def compute_nme(t, that):
    """
    Compute the Normalized Mean Error
    Parameters
    ----------
    t : Array like, size (n_targets, n_samples)
    True target value

    that : Array like, size (n_targets, n_samples)
    Predicted target value

    Returns
    -------
    Normalized Mean Error
    """
    try:
        assert(t.shape == that.shape)
    except AssertionError:
        print("Shape inconsistency:", t.shape, that.shape)
        sys.exit(1)
    return 20 * np.log10(np.linalg.norm(t - that, 'fro') / np.linalg.norm(t, 'fro'))


def admm_solver(x, y, eps_o, mu, kmax):
    p, N = x.shape
    Q, N = y.shape
    Lam = np.zeros((Q, p))
    xxt = np.matmul(x, x.transpose())
    yxt = np.matmul(y, x.transpose())
    tmp = np.linalg.inv(xxt + 1 / mu * np.identity(p))
    Z = Lam
    cost = []

    for _ in range(kmax):
        O = np.matmul(yxt + (1 / mu) * (Z + Lam), tmp)
        Z = O - Lam
        nZ = np.linalg.norm(Z, 'fro')
        if nZ > eps_o:
            Z = Z * (eps_o / nZ)
        Lam = Lam + Z - O
        cost.append(compute_nme(y, np.matmul(O, x)))
        if len(cost) > 2 and cost[-2] <= cost[-1]:
            break

    if len(cost) == kmax:
        print("\n\n:::::WARNING:::: admm_solver Did not converged.\n\n")

    #plt.plot(cost)
    return O



if __name__ == "__main__":
    from sklearn.datasets import load_digits
    X, T = load_digits(return_X_y=True)
    X, T, Xt, Tt = div_dataset(X, T, test_perc=.2, saxis=0)
    ntest = Xt.shape[0]

    dkf = DKF(nnodes=500, lmax=3)
    dkf.fit(X.T, T.T)


    y = dkf.forward(X.T)
    xhat = dkf.invert(y)

    embedding = UMAP().fit_transform(X)
    plt.figure()
    plt.scatter(embedding[:, 0], embedding[:, 1], c=T.reshape(-1))

    embedding = UMAP().fit_transform(y)
    plt.figure()
    plt.scatter(embedding[:, 0], embedding[:, 1], c=T.reshape(-1))



    np.allclose(X, dkf.invert(y), atol=1e-10)
    print("here")

    dkf = DKF(nnodes=500, lmax=3)
    dkf.fitCV(X.T, T.T, alphas=[10 ** i for i in range(-1, 15)], enplot=True, vperc=ntest / X.shape[0])
    fname = "DKF_r2_score_vs_lambda.png"
    plt.savefig(fname)
    print(fname)

"""
    plt.figure()
    for lmax in range(3, 10):
        dkf.append(DKF(nnodes=500, lmax=lmax))
        dkf[-1].fitCV(X.T, T.T, alphas=[10**i for i in range(-1, 15)], enplot=False, vperc=ntest/X.shape[0])
        plt.plot(np.log10(tkf[-1].vperf["alphas"]), tkf[-1].vperf["val_score"], label=str(lmax))
        plt.legend()
        plt.pause(0.0001)

    print("Here.")

"""