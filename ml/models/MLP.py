"""
Created on 02 Aug 2018

@author: anthon
"""


from sklearn.neural_network.multilayer_perceptron import MLPRegressor



class MLP(MLPRegressor):
    """Multi Layer Perceptron.

    Wrapper for sklearn.neural_network.multilayer_perceptron. MLPRegressor which adds userdata field.

    Parameters
    ----------
    See scikit-learn documentation.
    """
    def __init__(self, val_idx=None, **kwargs):
        MLPRegressor.__init__(self, **kwargs)
        self.name = "MLP"
        self.val_idx = val_idx
        #self.userdata = {"name": "ffNet", "arch": str((self.hidden_layer_sizes[0], len(self.hidden_layer_sizes))), "reg": mathlatex(alpha2str(self.alpha))}
