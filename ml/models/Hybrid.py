'''
Created on 16 May 2018

@author: anthon
'''
import torch
import torch.nn as nn
from torch.autograd import Variable

import numpy as np
from .DKN import DKN
from .ffNet import ffNet
from itertools import chain
from .myLSTM import myLSTM



class Hybrid(nn.Module):
    '''
        Integrate time series
    '''
    
    def __init__(self, input_size, hidden_size, ts_dim, ts_hidden_size, num_output, front_end="ffNet"):
        super(Hybrid, self).__init__()      
        self.input_size=    input_size;
        self.hidden_size=   hidden_size;
        self.num_output=    num_output;
    
        if front_end == "DKN":
            self.front_end= DKN(input_size, hidden_size, num_output);
        else:
            self.front_end= ffNet(input_size, hidden_size, num_output);
    
        self.ts_dim=        ts_dim;
        self.ts_hidden_size=ts_hidden_size;
        self.back_end=      myLSTM(ts_dim, ts_hidden_size, num_output, 1);
    
        self.optimizer=     torch.optim.Adam( chain(self.front_end.parameters(),self.back_end.parameters()), lr=0.01);
        self.criterion=     nn.MSELoss();

    def forward(self, x_ts, x):
        # print(x_ts.shape)
        _,h=            self.back_end.forward_full_ts(x_ts)
        hiddenState=    h[0].squeeze()
        #print(hiddenState.mapfiles.shape,   x.mapfiles.shape)
        feats=          Variable( torch.cat((hiddenState.data, x.data),dim=0).view(-1) )
        #print( feats)
        return self.front_end.forward( feats )
    
    def run_batch(self,X,ts_X,T):
        batch_loss=0;
        N=X.shape[1]
        That=T.clone()
        
        for isample in range(N):
            x=  X[:,isample];
            ts= ts_X[:,:,isample];
            t=  T[:,isample];
            
            #self.optimizer.zero_grad();
            that= self.forward(ts,x);
            loss= self.criterion(that,t);
            #loss.backward()
            #self.optimizer.step()
            
            batch_loss+=loss.data[0]
            That[:,isample]=that;
        
        return np.round(batch_loss/N,3),That  
    
    def run_epoch(self,X,ts_X,T):
        epoch_loss=0;
        N=X.shape[1]
        That=T.clone()
        
        for isample in range(N):
            x=  X[:,isample];
            ts= ts_X[:,:,isample];
            t=  T[:,isample];
            
            self.optimizer.zero_grad();
            that= self.forward(ts,x);
            loss= self.criterion(that,t);
            loss.backward()
            self.optimizer.step()
            
            epoch_loss+=loss.data[0]
            That[:,isample]=that;
        
        return np.round(epoch_loss/N,3),That    
 