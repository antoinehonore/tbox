"""
Created on 16 May 2018

@author: anthon
"""

from sklearn.linear_model.ridge import RidgeCV, Ridge


class RLS(Ridge):
    """
    Regularized Least-Square.

    Wrapper for sklearn.linear_model.ridge.RidgeCV object.

    Parameters
    ----------
    See scikit-learn documentation.
    """

    def __init__(self, val_idx=None, **kwargs):
        Ridge.__init__(self, **kwargs)
       # self.userdata = {"name": "RLS", "arch" : " ", "reg": mathlatex(alpha2str(kwargs["alpha"]))}
        self.name = "RLS"
        self.val_idx = val_idx

class RLScv(RidgeCV):
    def __init__(self, val_idx=None, **kwargs):
        RidgeCV.__init__(self, **kwargs)
        self.userdata = {"name": "RLScv", "arch": " ", "reg": "cv"}
        self.name = "RLS"
        self.val_idx = val_idx

'''
#Deprecated
class RLS(object):
    def __init__(self, LAMBDA, K):
        self.LAMBDA = LAMBDA
        self.K = K
        self.name = "RLS"


    def reset(self):
        self.__init__(self.LAMBDA, self.K)


    def regLstSq(self, X, T, lam):
        d, n = X.shape
        return np.matmul(np.matmul(np.linalg.inv(np.matmul(X, X.transpose()) + lam * n * np.eye(d)), X),
                         T.transpose()).transpose()


    def par_lstsq(self, indata):
        #:param indata: List [Xtrain,Ttrain,Xval,Tval]
        #:return: List size (len(self.LAMBDA)) [validation error for each lambda ]
        
        W = [self.regLstSq(indata[0], indata[1], lamb) for lamb in self.LAMBDA]
        val_nme = [compute_nme(indata[3], np.matmul(w, indata[2])) for w in W]
        return val_nme

    def learningCurve(self, X, T, xval=None, tval=None, nmax=300):
        if xval is None and tval is None:
            xfold, tfold, _ = get_kfold(X, T, 8)
            xtrain, ttrain, Xval, Tval = get_train_cv_sets(xfold, tfold, 3)
        else:
            xtrain, ttrain, Xval, Tval = X, T, xval, tval


        m =         xtrain.shape[1]
        nlambda =    len(self.LAMBDA)

        xx =        list(range(1, nmax, 10))
        assert (xx[-1] < m)
        error_train =   np.empty((len(xx), nlambda))
        error_val =     np.empty((len(xx), nlambda))

        for ilambda in range(nlambda):
            for k, i in enumerate(xx):
                theta = self.regLstSq(xtrain[:, :i], ttrain[:, :i], self.LAMBDA[ilambda])

                error_train[k, ilambda] = compute_nme(ttrain[:, :i], np.matmul(theta, xtrain[:, :i]))
                error_val[k, ilambda] = compute_nme(Tval, np.matmul(theta, Xval))

            # Plot
            plt.figure()
            plt.plot(xx, error_train[:, ilambda], label="Train error")
            plt.plot(xx, error_val[:, ilambda], label="Val error")
            plt.title("Learning Curves, model: " + self.name + " lambda:" + str(self.LAMBDA[ilambda]))
            plt.legend()
            plt.pause(0.0001)
            # plt.savefig("LC_"+ self.name + " lambda:"+str(self.LAMBDA[ilambda])+ '.jpg')
            #plt.close()

        if nlambda > 1:
            plt.figure()
            plt.plot(np.log10(self.LAMBDA), error_train[-1, :], label="Train error")
            plt.plot(np.log10(self.LAMBDA), error_val[-1, :], label="Val error")
            plt.xlabel("log10-Lambda")
            plt.title("Val error vs lambda, model: " + self.name)
            plt.legend()
            #plt.savefig("regCurve_" + self.name + " lambda:" + str(self.LAMBDA[ilambda]) + '.jpg')
            plt.pause(0.0001)
        return error_train, error_val


    def train(self, X, T, Xt, Tt, xval=None,tval=None,plot_res=False,verbose=0):
        LAMBDA = self.LAMBDA
        K = self.K

        xfolds, tfolds, _ = get_kfold(X, T, K)
        val_nme = np.empty((K, len(LAMBDA)))


        for k in range(K):
            # Get training and validation mapfiles
            indata = [get_train_cv_sets(xfolds, tfolds, k)]

            #Run on k-th fold
            val_nme[k, :] = self.par_lstsq(indata[0])

        # Get the lambda performing the best on the cv set
        ibest = np.argmin(np.mean(val_nme, axis=0))
        if plot_res:
            plt.figure()
            plt.plot(np.log10(self.LAMBDA), np.mean(val_nme, axis=0))
            plt.pause(0.001)

        # Perform RLS on the full training set with the optimal lambda
        w = self.regLstSq(X, T, LAMBDA[ibest])
        best_Tthat = np.matmul(w, Xt)
        best_That = np.matmul(w, X)

        results = {"T_hat": best_That, "Tt_hat": best_Tthat, "extra": [LAMBDA[ibest], w]}

        return results
'''