'''
Created on 03 Aug 2018

@author: anthon
'''


import numpy as np
import sklearn


def alpha2str(alpha):
    return "10^{" + str(int(np.log10(alpha)))+"}" if alpha > 0 else "10^{-\inf}"


def mathlatex(str_):
    return "$"+str_+"$"


def rbf_(x, y, gamma=None, n_jobs=None, out=None):
    pairdist = sklearn.metrics.pairwise_distances(x, y)
    np.square(pairdist, out=pairdist)
    if gamma is None:
        gamma = (pairdist.sum() / pairdist.shape[1] / pairdist.shape[0]) ** (-1)
    return np.exp(-gamma * pairdist, out=pairdist), gamma
