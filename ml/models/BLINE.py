"""
Created on 16 May 2018

@author: anthon
"""


import numpy as np
from sklearn.metrics.scorer import r2_score


class BLINE(object):
    """
    Dummy baseline algorithm which always returns the average of the training data targets.
    """

    def __init__(self, metric=r2_score):
        self.metric = metric
        self.name = "BLINE"
        pass

    def fit(self, X, T):
        """
        Fit the model to the data in argument.

        Parameters
        ----------
        X : numpy.ndarray size (n_samples, n_features)
        Training data matrix.

        T : numpy.ndarray size (n_samples, n_targets)
        Training target matrix.

        """
        self.mu_train = T.mean(0)
        return self

    def predict(self, x):
        #  Always return the average of training data
        return np.ones(x.shape[0]).reshape(-1, 1) @ self.mu_train.reshape(1, -1)

    def score(self, x, t):
        that = self.predict(x)
        return self.metric(t, that)