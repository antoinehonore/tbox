"""
Created on 16 May 2018

@author: anthon
"""


import numpy as np
import sys
from tbox.utils import pidprint
from tbox.ml.utils import normc
from sklearn.metrics.scorer import r2_score
from tbox.ml.models.utils import rbf_



class DKN(object):
    """
    Dense Kernel Network.

    Parameters
    ----------
    nnodes : int, optional, default: 1000
    Number of random nodes at each layer.

    lmax : int, optional, default: 10
    Maximum number of layers.

    alphas : tuple, optional, default: 10^-2..10^3
    Regularization parameters of the kernel regressions.

    K : int, optional, default: 3
    Number of folds used for cross-validation.
    If K == 1 : The cross-validation is skipped and the first element of the alphas tuple is used at each layer.

    metric : Callable, optional, default: sklearn.metric.scorer.r2_score
    Scoring function.

    Attributes
    ----------
    map_to_target : list of numpy.ndarray
    Matrices used for mapping to target space.

    fulltrain_kern : list of numpy.ndarray
    Training data kernel at each layer.

    input_fulltrain : list of numpy.ndarray
    Store the training data input at each layer. This is to be able to compute the data kernel at prediction time.

    total_number_layer : int
    Increase by 1 each time a layer is added to the network.

    userdata : dictionary
    Stores information about the model: name, architecture, regression parameter.

    status : str
        'Initialized' : Is set after initialization
        'Converged' : Is set after fitting is done. If status == 'Converged' and the method fit() is called, the model
            is reinitialized before fitting.

    R1 : numpy.ndarray, size (d_features, nnodes)
    Input layer random matrix.

    R : list, length lmax - 1
    Hidden layers random matrix.

    VQ : numpy.ndarray, size (2 * n_targets, n_targets)
    Target estimator transformation, inherited from PLN.

    """

    def __init__(self, alphas=tuple([10**i for i in range(-5, 6)]), metric=r2_score, nnodes=1000,\
                 lmax=10, K=3, delta=50, val_idx=None, verbose=True):

        self.nnodes = nnodes
        self.lmax = lmax
        self.alphas = alphas
        self.metric = metric
        self.delta = delta
        self.val_idx = val_idx

        #  Keep data in memory for prediction
        self.map_to_target = [0 for _ in range(lmax)]
        self.fulltrain_kern = [0 for _ in range(lmax)]
        self.input_fulltrain = [0 for _ in range(lmax)]

        self.train_data = None
        self.train_label = None
        self.train_perf = [0 for _ in range(self.lmax)]

        self.A = [None for _ in range(self.lmax)]
        self.y = [None for _ in range(self.lmax)]
        self.K = [None for _ in range(self.lmax)]
        self.gamma = [0 for _ in range(self.lmax)]


        self.tval_hat = np.zeros((self.lmax, len(self.alphas)))
        metric_str = str(metric)

        if "r2_score" in metric_str or "accuracy" in metric_str:
            self.choice_fun = np.argmax
            self.inf_perf = -np.inf

        elif "nme" in metric_str:
            self.choice_fun = np.argmin
            self.inf_perf = np.inf

        else:
            print("unknown metric function. Exit.", metric)
            sys.exit(1)

        self.name = "DKN"
        self.total_number_layers = 0
        self.verbose=verbose

    def improvement(self, old, new):
        if self.choice_fun == np.argmin:
            return new < old

        if self.choice_fun == np.argmax:
            return new > old

    def fit(self, X, T, vperc=.15):
        """
        Fit the model to the data in argument.

        Parameters
        ----------
        X : numpy.ndarray size (n_samples, n_features)
        Training data matrix.

        T : numpy.ndarray size (n_samples, n_targets)
        Training target matrix.

        """

        N, Q = T.shape
        _, d = X.shape

        self.VQ = np.concatenate((np.eye(Q), -np.eye(Q)), axis=0)
        self.alpha = [0 for _ in range(self.lmax)]
        self.TEMP = [0 for _ in range(self.lmax)]


        if self.val_idx is None:
            val_idx = np.zeros(N, dtype=np.bool)
            val_idx[np.random.permutation(N)[:int(N * vperc)]] = True

        else:
            val_idx = self.val_idx


        train_idx = ~val_idx
        nval = val_idx.sum()
        ntrain = N - nval


        tval_hat = np.zeros((nval, Q))
        ttrain_hat = np.zeros((ntrain, Q))
        full_ttrain_hat = np.zeros((N, Q))
        self.train_data = X
        self.train_label = T

        y_in = self.train_data
        K_in, gamma = self.kernel(self.train_data, self.train_data)
        if self.verbose:
            pidprint("Kernel size:", K_in.nbytes/1024/1024,"MB")
        self.gamma_init = gamma
        self.val_perf = np.zeros(self.lmax)
        self.train_perf = np.zeros_like(self.val_perf)

        for l in range(self.lmax):
            layer_val_perf = np.zeros(len(self.alphas))
            layer_train_perf = np.zeros_like(layer_val_perf)
            layer_val_perf[:] = self.inf_perf
            layer_train_perf[:] = self.inf_perf




            #   1-fold cross validation
            Kval = K_in[train_idx, :][:, val_idx]
            Ktrain = K_in[train_idx, :][:, train_idx]
            BUFF = np.zeros((N, Q))

            if self.verbose:
                pidprint("l:", l, "Cross-validation...")



            for ialpha, alpha in enumerate(self.alphas):
                if self.verbose:
                    pidprint("")
                    pidprint("l:", l, "i:", ialpha, "log10-alpha:", np.log10(alpha))
                    pidprint("l:", l, "i:", ialpha, "inv and val hat")
                np.matmul(Kval.T, self.solve_regression(Ktrain, T[train_idx, :], alpha, out=BUFF[:ntrain, :]), out=tval_hat)
                layer_val_perf[ialpha] = self.metric(T[val_idx, :], tval_hat)

                if self.verbose:
                    pidprint("l:", l, "i:", ialpha, "val_e:", layer_val_perf[ialpha])

                if self.verbose:
                    pidprint("l:", l, "i:", ialpha, "train hat")
                np.matmul(Ktrain.T, BUFF[:ntrain, :], out=ttrain_hat)
                layer_train_perf[ialpha] = self.metric(T[train_idx, :], ttrain_hat)

                if self.verbose:
                    pidprint("l:", l, "i:", ialpha, "train_e:", layer_train_perf[ialpha])

                if ialpha >= 1 and (not self.improvement(layer_val_perf[ialpha-1], layer_val_perf[ialpha])):
                    break

            if self.verbose:
                pidprint("l:", l, "Cross-validation... done.")

            ibest = self.choice_fun(layer_val_perf)
            self.val_perf[l] = layer_val_perf[ibest]
            self.alpha[l] = self.alphas[ibest]


            if l == 0 or self.improvement(self.val_perf[l-1], self.val_perf[l]):   #  continues
                #   calculate ttrain_hat for propagation
                self.TEMP[l] = self.solve_regression(K_in, T, self.alpha[l], out=BUFF)
                np.matmul(K_in.T, self.TEMP[l], out=full_ttrain_hat)

                self.train_perf[l] = self.metric(T, full_ttrain_hat)

                #  tmp = self.score(self.train_data, self.train_label)

                self.A[l] = np.random.randn(y_in.shape[1], self.nnodes + self.delta * l)


                y_in = self.pln(y_in, self.A[l], full_ttrain_hat, self.VQ)
                self.y[l] = np.copy(y_in)

                if self.verbose:
                    pidprint("l:", l, "Compute kernel")

                Ktmp, gamma = self.kernel(y_in, y_in)

                self.gamma[l] = gamma
                K_in = np.add(Ktmp, K_in, out=K_in)

            else:
                self.y[l-1], self.A[l-1]= None, None
                break
        return self


    def solve_regression(self, K, T, alpha, out=None):
        n = K.shape[0]
        return np.matmul(np.linalg.inv(K + alpha * n * np.eye(n)), T, out=out)


    def predict(self, x):
        """
        Predict targets from feature vectors.

        Parameters
        ----------
        x : numpy.ndarray, size (n_samples, n_features)
        Feature vector to predict targets of.

        Returns
        -------
        that : numpy.ndarray, size (n_samples, n_targets)
        Matrix of predicted targets.
        """

        input_ = x
        data_kern = self.kernel(self.train_data, input_, gamma=self.gamma_init)
        for l in range(self.lmax):
            that = data_kern.T @ self.TEMP[l]
            if self.A[l] is None:
                break

            input_ = self.pln(input_, self.A[l], that, self.VQ)
            np.add(self.kernel(self.y[l], input_, gamma=self.gamma[l]), data_kern, out=data_kern)

        return that


    #   Kernel computation
    def kernel(self, x, y, gamma=None, out=None, n_jobs=None):
        K, gamma_out = rbf_(x, y, gamma=gamma, n_jobs=n_jobs, out=out)
        if gamma is None:
            return K, gamma_out
        else:
            return K


    def score(self, x, t):
        """
        Scores feature vectors prediction vs. feature vector target

        Parameters
        ----------
        x : numpy.ndarray, size (n_samples, n_features)
        Feature vector to score the predict targets of.

        t : numpy.ndarray, size (n_samples, n_targets)
        Target vectors associated with x.

        Returns
        -------
        scores : numpy.ndarray (n_samples,)
        Scoring of the predictions of input feature vectors.
        """
        that = self.predict(x)
        return self.metric(t, that)


    def normalize(self, x):
        return normc(x, axis=1)  #   np.multiply( np.sqrt(    np.divide( np.square(x) , np.sum(np.square(x),axis=0) )    ),  np.sign(x))


    def pln(self, x, R, that, VQ):
        Z = np.concatenate((that @ VQ.T, self.normalize(x @ R)), axis=1)
        np.maximum(0, Z, out=Z) # + .1 * np.minimum(0, Z)
        return Z


if __name__ == "__main__":
    from sklearn import datasets

    diabetes = datasets.load_diabetes()
    n = 300
    X = diabetes.data[:n]
    y = diabetes.target[:n].reshape(-1, 1)
    Xt = diabetes.data[n:]
    yt = diabetes.target[n:].reshape(-1, 1)

    model = DKN(nnodes=2000)
    model.fit(X, y)
    yhat = model.predict(X)

    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(y)
    plt.plot(yhat)
    plt.pause(0.00001)
    print(model.score(X, y))
